package texteditorfx;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.*;
import javafx.event.*;
import javafx.stage.FileChooser;
import javafx.print.PrinterJob;


/**
 *
 * @author lavie
 */
public class TextEditorFX extends Application {
    
    private BorderPane racine;
    private TextArea textArea;
    private MenuBar menuBar;
    private Menu m1,m2,m3,m4;
    private ArrayList<MenuItem> mi1,mi2;
    private String texte;
    private Scanner lecture;
    private File actualFile =null;
    
    public void start(Stage primaryStage) {
        
        // Create MenuBar
        menuBar = new MenuBar();
        
        // Create menus
        m1 = new Menu("Fichier");
        m2 = new Menu("Edition");
        m3 = new Menu("Outils");
        m4 = new Menu("Aide");

        // Add Menus to the MenuBar
        menuBar.getMenus().addAll(m1, m2, m3,m4);
        
        // Create list of MenuItems
        mi1 = new ArrayList<>();
        mi2 = new ArrayList<>();
        // Create MenuItems
        
        mi1.add(new MenuItem("Nouveau"));
        mi1.add(new MenuItem("Ouvrir"));
        mi1.add(new MenuItem("Sauvegarder"));
        mi1.add(new MenuItem("Sauvegarder sous"));
        mi1.add(new MenuItem("Imprimer"));
        mi1.add(new MenuItem("Quitter"));
        
        //add submenu in menu
        m1.getItems().addAll(mi1);
        
        // Create MenuItems
        mi2.add(new MenuItem("Couper"));
        mi2.add(new MenuItem("Copier"));
        mi2.add(new MenuItem("Coller"));
        mi2.add(new MenuItem("Trouver"));
        mi2.add(new MenuItem("Trouver suivant"));
        mi2.add(new MenuItem("Remplacer"));
        mi2.add(new MenuItem("Tout sélectionner"));
        
        //add submenu in menu
        m2.getItems().addAll(mi2);
        
        m4.getItems().add(new MenuItem("A propos de"));
        
        for (int i=0;i<mi1.size();i++)
        mi1.get(i).setOnAction(e -> setHandlers(e));
        
        for (int i=0;i<mi2.size();i++)
        mi2.get(i).setOnAction(e -> setHandlers(e));
        
        m4.getItems().get(0).setOnAction(e -> setHandlers(e));
        //Create textarea
        textArea = new TextArea();
        //Create BorderPane
        racine = new BorderPane();
        //Add to BorderPane
        racine.setTop(menuBar);
        racine.setCenter(textArea);
        Scene scene = new Scene(racine ,600, 500);
        
        primaryStage.setTitle("Editeur de texte");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    private void setHandlers(ActionEvent event)
    {
        //New
        if(event.getSource()==mi1.get(0))
        {
            TextArea NouveautextArea = new TextArea();
            textArea = NouveautextArea;
            racine.setCenter(NouveautextArea);
        }
        
        //Open
        if(event.getSource()==mi1.get(1))
        {
            texte="";
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("HTML Files", "*.htm"));
            File fichierChoisi =fileChooser.showOpenDialog(null);
            actualFile=fichierChoisi;
            if(fichierChoisi != null)
            {
              textArea.clear();
                try {
                    lecture = new Scanner(fichierChoisi);
                    while (lecture.hasNext()) {
                        String doc = lecture.nextLine();
                        texte=texte+doc+"\n";
                    }
                } catch (Exception e){e.printStackTrace();}
                textArea.setText(texte);   
            }
        }
        
        //Save
        if(event.getSource()==mi1.get(2))
        {
            if(actualFile==null)
            {
                mi1.get(3).fire();
            }
            else
            {
            try
            { 
                FileWriter lu = new FileWriter(actualFile);
                BufferedWriter out = new BufferedWriter(lu);
                out.write(textArea.getText());
                out.close();
                } catch (Exception e) {e.printStackTrace();}
            }
        }
        //Save as
        if(event.getSource()==mi1.get(3))
        {
            
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Enregistrer sous");
            File monFichier =fileChooser.showSaveDialog(null);
            try
            { 
            FileWriter lu = new FileWriter(monFichier);
            BufferedWriter out = new BufferedWriter(lu);
            out.write(textArea.getText());
            out.close();
            } catch (Exception e) {e.printStackTrace();}
            actualFile=monFichier;
        }
        // Impress
        if(event.getSource()==mi1.get(4))
        {
            PrinterJob printerJob = PrinterJob.createPrinterJob();
            if (printerJob.showPrintDialog(textArea.getScene().getWindow())) {
            if (printerJob.printPage(textArea)) 
              printerJob.endJob();
            }
        }
        //Quit 
        if(event.getSource()==mi1.get(5))
        {
            textArea.getScene().getWindow().hide();
        }
        //Cut
        if(event.getSource()==mi2.get(0))
        {
            textArea.cut();
        }
        //Copy
        if(event.getSource()==mi2.get(1))
        {
            textArea.copy();
        }
        //Paste
        if(event.getSource()==mi2.get(2))
        {
            textArea.paste();
        }
        //Find word
        if(event.getSource()==mi2.get(3))
        {
            //Create a Box of dialog
            TextInputDialog dialog = new TextInputDialog();
            dialog.setHeaderText("Mot à trouver");
            dialog.setTitle("FindWord");
            //get the word given
            Optional<String> mot = dialog.showAndWait();
            //if a word is given
            if(mot.isPresent())
            {
                String resultat=mot.get();
                int p1= textArea.getText().indexOf(resultat);
                int p2= resultat.length();
                //No word find
                if (p1 == -1) 
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("FindWord");
                    alert.setHeaderText(null);
                    alert.setContentText("Ce mot n'est pas présent dans le texte.");
                    alert.showAndWait();
                }
                //select the word find
                else
                {
                    textArea.selectRange(p1,p1+p2);
                }
            }
        }
        if(event.getSource()==mi2.get(4))
        {
            String selection = textArea.getSelectedText();
            if(selection != null)
            {
                int p1= textArea.getText().indexOf(selection,textArea.getCaretPosition()+selection.length()+1);
                int p2= selection.length();
                textArea.selectRange(p1,p1+p2);
            }
        }
        
        if(event.getSource()==mi2.get(5))
        {
            TextInputDialog WordToReplace = new TextInputDialog();
            WordToReplace.setHeaderText("Mot à remplacer");
            WordToReplace.setTitle("Replace");
            Optional<String> motaRemplacer = WordToReplace.showAndWait();
            
            TextInputDialog newWord = new TextInputDialog();
            newWord.setHeaderText("Nouveau mot");
            newWord.setTitle("Replace");
            Optional<String> nouveauMot = newWord.showAndWait();
            if(motaRemplacer.isPresent())
            {
                String motA=motaRemplacer.get();
                int p1= textArea.getText().indexOf(motA);
                int p2= motA.length();
                if (p1 == -1) 
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("FindWord");
                    alert.setHeaderText(null);
                    alert.setContentText("Ce mot n'est pas présent dans le texte.");
                    alert.showAndWait();
                }
                else
                {
                    String newMot =nouveauMot.get();
                    textArea.setText(textArea.getText().replaceAll(motA,newMot));
                } 
            }
        } 
        //Select All
        if(event.getSource()==mi2.get(6))
        {
            textArea.selectAll();
        } 
        //Helpmenu
        if(event.getSource()==m4.getItems().get(0))
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Aide");
            alert.setHeaderText("Ce projet vous est présenté par Antonin Lavie.");
            alert.setContentText("Nouveau: supprime le ficher actuellement ouvert et en ouvre un nouveau (sauvegarder le fichier en cours avant d’effectuer cette action)\n" +
"Ouvrir: Pour l’instant n'ouvre que des fichiers .txt \n" +
"Sauvegarder / Sauvegarder sous: si le fichier est déjà sauvegardé quelques pars, enregistre par-dessus la dernière sauvegarde, dans l'autre cas ouvre une fenêtre afin de sauvegarder le fichier (ne pas oublier le format .txt dans le nom du fichier)");
            alert.showAndWait();
        } 
    }
    public static void main(String[] args) {
        launch(args);
    }   
}
